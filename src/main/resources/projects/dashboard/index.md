## Features

* Various widgets like calendar, clock and web view are available
* Arrange widgets on your dashboard as you want them to look like
* Easily configurable using a YAML file
* Extendable with your own widgets

## Contained widgets

| Module name       | Description                                                                                     |
|-------------------|-------------------------------------------------------------------------------------------------|
| calendar          | Display events from one or more calendars retrieved from a CalDAV server                        |
| clock             | An analogue clock                                                                               |
| container         | Display multiple child widgets aligned in a row or a column                                     |
| groupbox          | Display a single child widget in a group box containing a title                                 |
| media_player      | Play any stream or video supported by QMediaPlayer                                              |
| media_player_info | See what's currently playing in any media player supporting MPRIS                               |
| nexcloud_news     | Show news from the Nextcloud News app                                                           |
| pushover          | Show realtime push notifications from Pushover as a list                                        |
| soundboard        | Show buttons to play sounds                                                                     |
| splitter          | Like "container" but allows to resize the widgets by dragging the spliter                       |
| tabs              | Display multiple child widgets using tabs                                                       |
| tasks             | Display tasks from one or more task lists retrieved from a CalDAV server                        |
| timer             | Allows you to define a timer (hours, minutes and seconds) and plays a sound once it has elapsed |
| vlc               | Play any stream or video supported by VLC media player                                          |
| web_view          | Display any web page                                                                            |