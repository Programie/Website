## Installation

Install the package using `composer require programie/jsonconfig`.

## Usage

See the [wiki](https://github.com/Programie/JsonConfig/wiki) for information how to use this library.